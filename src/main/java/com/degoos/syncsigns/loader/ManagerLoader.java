package com.degoos.syncsigns.loader;

import com.degoos.syncsigns.manager.*;

import java.util.HashMap;
import java.util.Map;

public class ManagerLoader {

    private static Map<Class<?>, Manager> managers;
    private static boolean loaded = false;


    public static void load() {
        managers = new HashMap<>();
        loadDefManagers();
    }


    private static void loadDefManagers() {
        if (loaded) return;
        addManager(new FileManager());
        addManager(new DatabaseManager());
        addManager(new FormatManager());
        addManager(new SignManager());
        loaded = true;
    }


    public static void addManager(Manager manager) {
        managers.put(manager.getClass(), manager);
        manager.load();
    }


    @SuppressWarnings("unchecked")
    public static <T extends Manager> T getManager(Class<T> clazz) {
        Manager man = managers.get(clazz);
        if (man == null) return null;
        return (T) man;
    }

}
