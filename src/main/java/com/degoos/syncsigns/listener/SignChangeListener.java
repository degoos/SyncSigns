package com.degoos.syncsigns.listener;


import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.manager.FormatManager;
import com.degoos.syncsigns.manager.SignManager;
import com.degoos.syncsigns.object.SignFormat;
import com.degoos.syncsigns.object.SyncSign;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.block.WSBlockBreakEvent;
import com.degoos.wetsponge.event.block.WSSignChangeEvent;
import com.degoos.wetsponge.text.WSText;

import java.util.Optional;

public class SignChangeListener {

	@WSListener
	public void onSignChange (WSSignChangeEvent event) {
		if (!event.getLine(0).toFormattingText().equalsIgnoreCase("[syncsigns]") || !event.getPlayer().hasPermission("syncSigns.addSign")) return;
		Optional<SignFormat> optional = ManagerLoader.getManager(FormatManager.class).getFormat(event.getLine(2).toFormattingText());
		if (!optional.isPresent()) {
			event.getPlayer().sendMessage(WSText.builder(WSText.of("This sign format doesn't exist!")).color(EnumTextColor.DARK_RED).build());
			return;
		}
		ManagerLoader.getManager(SignManager.class).addSign(new SyncSign(event.getBlock().getLocation(), event.getLine(1).toFormattingText(), optional.get()));
		event.setLine(0, WSText.of(""));
		event.setLine(1, WSText.of("..."));
		event.setLine(2, WSText.of(""));
		event.setLine(3, WSText.of(""));
	}


	@WSListener
	public void blockBreak (WSBlockBreakEvent event) {
		ManagerLoader.getManager(SignManager.class).deleteSign(event.getLocation());
	}

}
