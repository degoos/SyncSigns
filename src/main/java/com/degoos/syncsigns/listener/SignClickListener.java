package com.degoos.syncsigns.listener;


import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.manager.SignManager;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractBlockEvent;

public class SignClickListener {

	@WSListener
	public void onSignClick(WSPlayerInteractBlockEvent.Secondary.MainHand event) {
		event.getBlockLocation()
			.ifPresent(position -> ManagerLoader.getManager(SignManager.class).getSign(position).ifPresent(sign -> sign.sendPlayerToServer(event.getPlayer())));
	}

}
