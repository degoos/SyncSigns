package com.degoos.syncsigns.object;


import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.manager.FileManager;
import com.degoos.syncsigns.manager.SignManager;
import com.degoos.syncsigns.tasks.UpdateTask;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.tileentity.WSTileEntitySign;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.world.WSLocation;

public class SyncSign {

    private WSLocation location;
    private String serverName;
    private SignFormat format;
    private SyncServer server;


    public SyncSign(WSLocation location, String serverName, SignFormat format) {
        this.location = location;
        this.serverName = serverName;
        this.format = format;
    }


    public WSLocation getLocation() {
        return location;
    }


    public void setLocation(WSLocation location) {
        this.location = location;
    }


    public String getServerName() {
        return serverName;
    }


    public void setServerName(String serverName) {
        this.serverName = serverName;
    }


    public SignFormat getFormat() {
        return format;
    }


    public void setFormat(SignFormat format) {
        this.format = format;
    }


    public SyncServer getServer() {
        return server;
    }


    public void setServer(SyncServer server) {
        this.server = server;
        format.updateSign(this);
    }


    public void updateSign() {
        if (this.location.getBlock().getTileEntity() instanceof WSTileEntitySign) format.updateSign(this);
        else ManagerLoader.getManager(SignManager.class).deleteSign(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SyncSign syncSign = (SyncSign) o;

        return location.equalsBlock(syncSign.location);
    }


    @Override
    public int hashCode() {
        return location.getBlockLocation().hashCode();
    }


    public void sendPlayerToServer(WSPlayer player) {
        new Thread(() -> {
            UpdateTask.updateServer(serverName);
            if (!canJoinToServer()) return;
            WetSponge.getBungeeCord().connectWSPlayer(player, getServerName());
            getServer().setData("PLAYERS", getServer().getInt("PLAYERS") + 1);
        }).start();
    }


    public boolean canJoinToServer() {
        if(server == null) return false;
        return getServer().isOnline() && (!getServer().hasWhiteList() || ManagerLoader.getManager(FileManager.class).getConfiguration().getBoolean("ignoreWhitelist"))
                && (!getServer().isFull() || ManagerLoader.getManager(FileManager.class).getConfiguration().getBoolean("ignoreFullServer")) ||
                getServer().canJoin();
    }
}
