package com.degoos.syncsigns.object;


import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.manager.FileManager;
import com.degoos.syncsigns.manager.SignManager;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.util.NumericUtils;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class SyncServer {

    private String name;
    private Map<String, String> data;
    private Timestamp lastUpdate;


    public SyncServer(String name, Map<String, String> data, Timestamp lastUpdate) {
        this.name = name;
        this.data = data;
        this.lastUpdate = lastUpdate;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Map<String, String> getData() {
        return new HashMap<>(data);
    }


    public String getData(String key) {
        return data.get(key);
    }


    public void setData(String key, Object value) {
        if (data.containsKey(key.toUpperCase())) data.remove(key.toUpperCase());
        data.put(key.toUpperCase(), value.toString());
    }


    public boolean getBoolean(String key) {
        return Boolean.valueOf(data.get(key.toUpperCase()));
    }


    public int getInt(String key) {
        String value = getData(key);
        if (!NumericUtils.isInteger(value)) return 0;
        return Integer.valueOf(value);
    }


    public Timestamp getLastUpdate() {
        return lastUpdate;
    }


    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }


    public boolean isOnline() {
        return lastUpdate.getTime() + 60000 > System.currentTimeMillis() || !getBoolean("ONLINE");
    }


    public boolean canJoin() {
        return getBoolean("CANJOIN");
    }


    public boolean hasWhiteList() {
        return getBoolean("WHITELIST");
    }


    public boolean isFull() {
        return getInt("MAXPLAYERS") <= getInt("PLAYERS");
    }


    public boolean allowsRandomJoin() {
        return getBoolean("ALLOWSRANDOMJOIN");
    }


    public void sendPlayerToServer(WSPlayer player) {
        if (!canJoinToServer()) return;
        WetSponge.getBungeeCord().connectWSPlayer(player, name);
        setData("PLAYERS", getInt("PLAYERS") + 1);
        ManagerLoader.getManager(SignManager.class).getSigns().stream().filter(sign -> sign.getServerName().equals(name)).forEach(sign -> sign.setServer(this));
    }


    public boolean canJoinToServer() {
        return isOnline() && (!hasWhiteList() || ManagerLoader.getManager(FileManager.class).getConfiguration().getBoolean("ignoreWhitelist"))
                && (!isFull() || ManagerLoader.getManager(FileManager.class).getConfiguration().getBoolean("ignoreFullServer")) || canJoin();
    }
}
