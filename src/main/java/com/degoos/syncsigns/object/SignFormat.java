package com.degoos.syncsigns.object;


import com.degoos.wetsponge.util.Validate;

import java.util.Map;

public class SignFormat {

    private String name;
    private String[] lines;


    public SignFormat(String name, String[] lines) {
        Validate.notNull(name, "Name cannot be null!");
        this.name = name;
        this.lines = lines;
    }


    public SignFormat(String name, String line1, String line2, String line3, String line4) {
        Validate.notNull(name, "Name cannot be null!");
        this.name = name;
        this.lines = new String[]{line1, line2, line3, line4};
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String[] getLines() {
        return lines;
    }


    public void setLines(String[] lines) {
        this.lines = lines;
    }


    public void updateSign(SyncSign sign) {
        sign.getLocation().updateSign(replaceString(lines, sign.getServer()));
    }


    private String[] replaceString(String[] lines, SyncServer server) {
        String[] newLines = new String[lines.length];
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            for (Map.Entry<String, String> entry : server.getData().entrySet()) line = line.replace("<" + entry.getKey().toUpperCase() + ">", entry.getValue());
            int motdIndex = 1;
            for (String motd : server.getData().get("MOTD").split(";")) {
                line = line.replace("<MOTD" + motdIndex + ">", motd);
                motdIndex++;
            }
            newLines[i] = line.replace("&", "§");
        }
        return newLines;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SignFormat that = (SignFormat) o;

        return name.equalsIgnoreCase(that.name);
    }


    @Override
    public int hashCode() {
        return name.toLowerCase().hashCode();
    }
}
