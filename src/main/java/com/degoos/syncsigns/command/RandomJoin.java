package com.degoos.syncsigns.command;


import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.manager.FileManager;
import com.degoos.syncsigns.object.SyncServer;
import com.degoos.syncsigns.tasks.UpdateTask;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RandomJoin extends WSCommand {

    public RandomJoin() {
        super("randomJoin", "Teleports you to a random server.");
    }


    @Override
    public void executeCommand(WSCommandSource wsCommandSource, String s, String[] strings) {
        if (!wsCommandSource.hasPermission("syncSigns.randomJoin") || !(wsCommandSource instanceof WSPlayer)) {
            wsCommandSource.sendMessage(WSText.builder("You don't have permission to use this command.").color(EnumTextColor.RED).build());
        } else {
            WSPlayer player = (WSPlayer) wsCommandSource;
            Set<SyncServer> servers = UpdateTask.getAllServers()
                    .stream()
                    .filter(server -> ManagerLoader.getManager(FileManager.class)
                            .getConfiguration().getStringList("randomJoinServers")
                            .contains(server.getName()) && server.canJoinToServer() && server.allowsRandomJoin())
                    .collect(Collectors.toSet());
            servers.stream().sorted((o1, o2) -> o2.getInt("PLAYERS") - o1.getInt("PLAYERS")).findFirst()
                    .ifPresent(syncServer -> syncServer.sendPlayerToServer(player));
        }
    }


    @Override
    public List<String> sendTab(WSCommandSource wsCommandSource, String s, String[] strings) {
        return new ArrayList<>();
    }
}
