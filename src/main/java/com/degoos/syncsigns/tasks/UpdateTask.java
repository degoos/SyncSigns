package com.degoos.syncsigns.tasks;

import com.degoos.syncsigns.SyncSigns;
import com.degoos.syncsigns.event.SyncSingDataRequestEvent;
import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.manager.DatabaseManager;
import com.degoos.syncsigns.manager.FileManager;
import com.degoos.syncsigns.manager.SignManager;
import com.degoos.syncsigns.object.SyncServer;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.server.WSServerListPingEvent;
import com.degoos.wetsponge.resource.B64;
import com.degoos.wetsponge.server.WSServerInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UpdateTask implements Runnable {

	@Override
	public void run() {
		SyncSingDataRequestEvent event = new SyncSingDataRequestEvent();
		WSServerInfo info = WetSponge.getServer().getServerInfo();

		WSServerListPingEvent motdEvent = new WSServerListPingEvent(info.getMotd(), info.getServerIcon(), info.getMaxPlayers(), null);
		WetSponge.getEventManager().callEvent(motdEvent);

		event.addData("WHITELIST", info.hasWhiteList());
		event.addData("PLAYERS", info.getOnlinePlayers());
		event.addData("MAXPLAYERS", info.getMaxPlayers());
		event.addData("MOTD", motdEvent.getDescription());
		event.addData("CANJOIN", true);
		event.addData("ONLINE", true);
		event.addData("ALLOWSRANDOMJOIN", true);
		WetSponge.getEventManager().callEvent(event);
		StringBuilder data = new StringBuilder();
		event.getData().forEach((key, value) -> data.append("--").append(B64.encode(key)).append("!!").append(B64.encode(value)));
		Connection database = DatabaseManager.getConnection();
		String serverName = ManagerLoader.getManager(FileManager.class).getConfiguration().getString("serverNameInBungee");

		try {
			PreparedStatement statement = database.prepareStatement("SELECT server FROM " + SyncSigns.TABLE + " WHERE server = ?");
			statement.setString(1, serverName);
			if (statement.executeQuery().next()) {
				statement.close();
				statement = database.prepareStatement("UPDATE " + SyncSigns.TABLE + " SET sign_data = ?, last_update = ? WHERE server = ?");
				statement.setString(1, data.toString().replaceFirst("--", ""));
				statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
				statement.setString(3, serverName);
				statement.executeUpdate();
				statement.close();
			} else {
				statement.close();
				statement = database.prepareStatement("INSERT INTO " + SyncSigns.TABLE + " (server, sign_data, last_update) VALUES (?, ?, ?)");
				statement.setString(1, serverName);
				statement.setString(2, data.toString().replaceFirst("--", ""));
				statement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
				statement.executeUpdate();
				statement.close();
			}
			Set<String> serverNames = new HashSet<>();
			Set<SyncServer> servers = new HashSet<>();
			ManagerLoader.getManager(SignManager.class).getSigns().forEach(sign -> serverNames.add(sign.getServerName()));
			serverNames.forEach(server -> {
				try {
					PreparedStatement signStatement = database.prepareStatement("SELECT * FROM " + SyncSigns.TABLE + " WHERE server = ?");
					signStatement.setString(1, server);
					ResultSet set = signStatement.executeQuery();
					if (set.next()) {
						Map<String, String> dataMap = new HashMap<>();
						for (String string : set.getString("sign_data").split("--"))
							dataMap.put(B64.decode(string.split("!!")[0]), B64.decode(string.split("!!")[1]));
						servers.add(new SyncServer(server, dataMap, set.getTimestamp("last_update")));
					}
					signStatement.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			});
			ManagerLoader.getManager(SignManager.class).getSigns()
				.forEach(sign -> servers.stream().filter(server -> server.getName().equals(sign.getServerName())).findAny().ifPresent(sign::setServer));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void sendFinalUpdate() {
		SyncSingDataRequestEvent event = new SyncSingDataRequestEvent();
		WSServerInfo info = WetSponge.getServer().getServerInfo();
		WSServerListPingEvent motdEvent = new WSServerListPingEvent(info.getMotd(), info.getServerIcon(), info.getMaxPlayers(), null);
		WetSponge.getEventManager().callEvent(motdEvent);

		event.addData("WHITELIST", info.hasWhiteList());
		event.addData("PLAYERS", info.getOnlinePlayers());
		event.addData("MAXPLAYERS", info.getMaxPlayers());
		event.addData("MOTD", motdEvent.getDescription());
		event.addData("CANJOIN", true);
		event.addData("ONLINE", true);
		event.addData("ALLOWSRANDOMJOIN", true);
		StringBuilder data = new StringBuilder();
		String serverName = ManagerLoader.getManager(FileManager.class).getConfiguration().getString("serverNameInBungee");
		event.getData().forEach((key, value) -> data.append("--").append(B64.encode(key)).append("!!").append(B64.encode(value)));
		Connection database = DatabaseManager.getConnection();
		try {
			PreparedStatement statement = database.prepareStatement("SELECT server FROM " + SyncSigns.TABLE + " WHERE server = ?");
			statement.setString(1, serverName);
			if (statement.executeQuery().next()) {
				statement.close();
				statement = database.prepareStatement("UPDATE " + SyncSigns.TABLE + " SET sign_data = ?, last_update = ? WHERE server = ?");
				statement.setString(1, data.toString().replaceFirst("--", ""));
				statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
				statement.setString(3, serverName);
				statement.executeUpdate();
			} else {
				statement.close();
				statement = database.prepareStatement("INSERT INTO " + SyncSigns.TABLE + " (server, sign_data, last_update) VALUES (?, ?, ?)");
				statement.setString(1, serverName);
				statement.setString(2, data.toString().replaceFirst("--", ""));
				statement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
				statement.executeUpdate();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static SyncServer updateServer(String server) {
		try {
			Connection database = DatabaseManager.getConnection();
			PreparedStatement signStatement = database.prepareStatement("SELECT * FROM " + SyncSigns.TABLE + " WHERE server = ?");
			signStatement.setString(1, server);
			ResultSet set = signStatement.executeQuery();
			if (set.next()) {
				Map<String, String> dataMap = new HashMap<>();
				for (String string : set.getString("sign_data").split("--"))
					dataMap.put(B64.decode(string.split("!!")[0]), B64.decode(string.split("!!")[1]));
				SyncServer updated = new SyncServer(server, dataMap, set.getTimestamp("last_update"));
				ManagerLoader.getManager(SignManager.class).getSigns().stream().filter(sign -> sign.getServer().getName().equals(updated.getName()))
					.forEach(sign -> sign.setServer(updated));
				return updated;
			}
			signStatement.close();
			return null;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}


	public static Set<SyncServer> getAllServers() {
		Set<SyncServer> servers = new HashSet<>();
		try {
			Connection database = DatabaseManager.getConnection();
			PreparedStatement signStatement = database.prepareStatement("SELECT * FROM " + SyncSigns.TABLE);
			ResultSet set = signStatement.executeQuery();
			while (set.next()) {
				Map<String, String> dataMap = new HashMap<>();
				for (String string : set.getString("sign_data").split("--"))
					dataMap.put(B64.decode(string.split("!!")[0]), B64.decode(string.split("!!")[1]));
				SyncServer updated = new SyncServer(set.getString("server"), dataMap, set.getTimestamp("last_update"));
				servers.add(updated);
				ManagerLoader.getManager(SignManager.class).getSigns().stream().filter(sign -> sign.getServer().getName().equals(updated.getName()))
					.forEach(sign -> sign.setServer(updated));
			}
			signStatement.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return servers;
	}


	public static void forceUpdate() {
		SyncSingDataRequestEvent event = new SyncSingDataRequestEvent();
		WSServerInfo info = WetSponge.getServer().getServerInfo();
		WSServerListPingEvent motdEvent = new WSServerListPingEvent(info.getMotd(), info.getServerIcon(), info.getMaxPlayers(), null);
		WetSponge.getEventManager().callEvent(motdEvent);

		event.addData("WHITELIST", info.hasWhiteList());
		event.addData("PLAYERS", info.getOnlinePlayers());
		event.addData("MAXPLAYERS", info.getMaxPlayers());
		event.addData("MOTD", motdEvent.getDescription());
		event.addData("CANJOIN", true);
		event.addData("ONLINE", true);
		event.addData("ALLOWSRANDOMJOIN", true);
		WetSponge.getEventManager().callEvent(event);
		StringBuilder data = new StringBuilder();
		String serverName = ManagerLoader.getManager(FileManager.class).getConfiguration().getString("serverNameInBungee");
		event.getData().forEach((key, value) -> data.append("--").append(B64.encode(key)).append("!!").append(B64.encode(value)));
		Connection database = DatabaseManager.getConnection();
		try {
			PreparedStatement statement = database.prepareStatement("SELECT server FROM " + SyncSigns.TABLE + " WHERE server = ?");
			statement.setString(1, serverName);
			if (statement.executeQuery().next()) {
				statement.close();
				statement = database.prepareStatement("UPDATE " + SyncSigns.TABLE + " SET sign_data = ?, last_update = ? WHERE server = ?");
				statement.setString(1, data.toString().replaceFirst("--", ""));
				statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
				statement.setString(3, serverName);
				statement.executeUpdate();
				statement.close();
			} else {
				statement.close();
				statement = database.prepareStatement("INSERT INTO " + SyncSigns.TABLE + " (server, sign_data, last_update) VALUES (?, ?, ?)");
				statement.setString(1, serverName);
				statement.setString(2, data.toString().replaceFirst("--", ""));
				statement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
				statement.executeUpdate();
				statement.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
