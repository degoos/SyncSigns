package com.degoos.syncsigns;


import com.degoos.syncsigns.command.RandomJoin;
import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.manager.DatabaseManager;
import com.degoos.syncsigns.manager.FileManager;
import com.degoos.syncsigns.tasks.UpdateTask;
import com.degoos.syncsigns.util.ListenerUtils;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.task.WSTask;

public class SyncSigns extends WSPlugin {

    private static SyncSigns instance;
    public static final String TABLE = "sync_signs";


    @Override
    public void onEnable() {
        instance = this;
        ListenerUtils.load();
        ManagerLoader.load();

        WetSponge.getCommandManager().addCommand(new RandomJoin());

        WSTask.of(task -> ManagerLoader.getManager(DatabaseManager.class).load()).runTaskTimerAsynchronously(6000, 6000, this);
        WSTask.of(new UpdateTask()).runTaskTimer(0,
                ManagerLoader.getManager(FileManager.class).getConfiguration().getInt("updateIntervalTicks"), this);
    }


    public static SyncSigns getInstance() {
        return instance;
    }

}
