package com.degoos.syncsigns.event;


import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.text.WSText;
import java.util.HashMap;
import java.util.Map;

public class SyncSingDataRequestEvent extends WSEvent {

	private Map<String, String> data;


	public SyncSingDataRequestEvent() {
		data = new HashMap<>();
	}


	public void addData(String key, Object value) {
		if (value instanceof WSText) value = ((WSText) value).toFormattingText();
		if (data.containsKey(key)) data.remove(key);
		data.put(key, value.toString());
	}


	public String getValue(String key) {
		return data.get(key);
	}


	public Map<String, String> getData() {
		return new HashMap<>(data);
	}

}
