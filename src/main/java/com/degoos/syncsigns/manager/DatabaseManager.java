package com.degoos.syncsigns.manager;


import com.degoos.syncsigns.SyncSigns;
import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.wetsponge.config.ConfigAccessor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager implements Manager {

	private static String username;
	private static String password;
	private static String url;


	public void load () {

		ConfigAccessor config = ManagerLoader.getManager(FileManager.class).getConfiguration();

		username = config.getString("database.username", "root");
		password = config.getString("database.password", "");
		url = replaceDatabaseString(config.getString("database.url", "'jdbc:sqlite:{DIR}{NAME}.db"));

		try (Connection connection = DriverManager.getConnection(url, username, password)) {
			this.createDB(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void createDB (Connection connection) {
		String query = "CREATE TABLE IF NOT EXISTS " +
		               SyncSigns.TABLE +
		               "(" +
		               "  server        VARCHAR(255) PRIMARY KEY," +
		               "  sign_data   TEXT(65535)," +
		               "  last_update TIMESTAMP" +
		               ");";
		try {
			Statement stmt = connection.createStatement();
			stmt.execute(query);
			DatabaseManager.sendCommit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public static Connection getConnection () {
		try {
			return DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static void sendCommit () {
		Connection connection = DatabaseManager.getConnection();
		if (connection == null) return;

		try {
			if (!connection.getAutoCommit()) connection.commit();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	private String replaceDatabaseString (String input) {
		input = input.replaceAll("\\{DIR\\}", SyncSigns.getInstance().getDataFolder().getPath().replaceAll("\\\\", "/") + "/");
		input = input.replaceAll("\\{NAME\\}", SyncSigns.getInstance().getId());

		return input;
	}
}
