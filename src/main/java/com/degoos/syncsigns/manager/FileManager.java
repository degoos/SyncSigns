package com.degoos.syncsigns.manager;


import com.degoos.syncsigns.SyncSigns;
import com.degoos.wetsponge.config.ConfigAccessor;

import java.io.File;

public class FileManager implements Manager {
    private ConfigAccessor configuration, formats, signs;


    public void load() {
        File pluginFolder = SyncSigns.getInstance().getDataFolder();

        configuration = new ConfigAccessor(new File(pluginFolder, "config.yml"));
        configuration.checkNodes(new ConfigAccessor(SyncSigns.getInstance().getResource("config.yml")), true);

        formats = new ConfigAccessor(new File(pluginFolder, "formats.yml"));
        signs = new ConfigAccessor(new File(pluginFolder, "signs.yml"));
    }


    public ConfigAccessor getConfiguration() {
        return configuration;
    }


    public ConfigAccessor getFormats() {
        return formats;
    }

    public ConfigAccessor getSigns() {
        return signs;
    }
}
