package com.degoos.syncsigns.manager;


import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.object.SignFormat;
import com.degoos.wetsponge.config.ConfigAccessor;

import java.util.*;

public class FormatManager implements Manager {

    private Set<SignFormat> formats;


    public void load() {
        formats = new HashSet<>();
        ConfigAccessor config = ManagerLoader.getManager(FileManager.class).getFormats();
        config.getKeys(false).forEach(name -> {
            List<String> lines = config.getStringList(name + ".lines");
            formats.add(new SignFormat(name, lines.size() < 1 ? "" : lines.get(0), lines.size() < 2 ? "" : lines.get(1), lines.size() < 3 ? "" : lines.get(2),
                    lines.size() < 4 ? "" : lines.get(3)));
        });
        loadDefault();
    }


    public void saveFormat(SignFormat format) {
        ConfigAccessor config = ManagerLoader.getManager(FileManager.class).getFormats();
        config.set(format.getName() + ".lines", Arrays.asList(format.getLines()));
        config.save();
    }


    public void addFormat(SignFormat format) {
        formats.add(format);
        saveFormat(format);
    }


    public Optional<SignFormat> getFormat(String name) {
        return formats.stream().filter(format -> format.getName().equalsIgnoreCase(name)).findAny();
    }


    public SignFormat getDefault() {
        return getFormat("default").get();
    }


    public void loadDefault() {
        if (getFormat("default").isPresent()) return;
        addFormat(new SignFormat("default", "&6&lSyncSigns", "<SERVERNAME>", "<MOTD1>", "<PLAYERS>/<MAXPLAYERS>"));
    }

}
