package com.degoos.syncsigns.manager;


import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.syncsigns.object.SignFormat;
import com.degoos.syncsigns.object.SyncSign;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.resource.B64;
import com.degoos.wetsponge.world.WSLocation;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class SignManager implements Manager {

	private Set<SyncSign> signs;


	public void load () {
		signs = new HashSet<>();
		ConfigAccessor config = ManagerLoader.getManager(FileManager.class).getSigns();
		config.getKeys(false).forEach(encodedLocation -> {
			String               location = B64.decode(encodedLocation);
			FormatManager        manager  = ManagerLoader.getManager(FormatManager.class);
			Optional<SignFormat> optional = manager.getFormat(config.getString(encodedLocation + ".format"));
			signs.add(new SyncSign(WSLocation.of(location), config.getString(encodedLocation + ".server"),
			                       optional.isPresent() ? optional.get() : manager.getDefault()));
		});
	}


	public void saveSign (SyncSign sign) {
		ConfigAccessor config   = ManagerLoader.getManager(FileManager.class).getSigns();
		String         location = B64.encode(sign.getLocation().toString());
		config.set(location + ".format", sign.getFormat().getName());
		config.set(location + ".server", sign.getServerName());
		config.save();
	}


	public void addSign (SyncSign sign) {
		signs.add(sign);
		saveSign(sign);
	}


	public Set<SyncSign> getSigns () {
		return new HashSet<>(signs);
	}


	public Optional<SyncSign> getSign (WSLocation location) {
		return signs.stream().filter(sign -> sign.getLocation().equalsBlock(location)).findAny();
	}


	public void deleteSign (WSLocation location) {
		getSigns().stream().filter(sign -> sign.getLocation().equalsBlock(location)).forEach(this::deleteSign);
	}


	public void deleteSign (SyncSign sign) {
		signs.remove(sign);
		ConfigAccessor config = ManagerLoader.getManager(FileManager.class).getSigns();
		config.set(B64.encode(sign.getLocation().toString()), null);
		config.save();
	}

}
