package com.degoos.syncsigns.manager;

import com.degoos.syncsigns.SyncSigns;
import com.degoos.syncsigns.loader.ManagerLoader;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManagerOld implements Manager {

    private Connection database;


    public void load() {
        try {
            openDatabase();
            Statement statement = database.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS syncsigns (server VARCHAR(255), data TEXT(1000000), last_update TIMESTAMP)");
            statement.close();
        } catch (Exception ex) {
            WetSponge.getServer().getConsole().sendMessage(WSText.builder("[SyncSigns] ERROR LOADING DATABASE. PLEASE CHECK IT").color(EnumTextColor.RED).build());
            ex.printStackTrace();
            WetSponge.getServer().shutdown();
        }

    }


    private void openDatabase() throws ClassNotFoundException, SQLException {
        ConfigAccessor config = ManagerLoader.getManager(FileManager.class).getConfiguration();

        String driver = config.getString("database.driver");
        String url = config.getString("database.url");
        String username = config.getString("database.username");
        String password = config.getString("database.password");
        Class.forName(driver);
        database = DriverManager.getConnection(replaceDatabaseString(url), username, password);
    }


    private static String replaceDatabaseString(String input) {
        input = input.replaceAll("\\{DIR\\}", SyncSigns.getInstance().getDataFolder().getPath().replaceAll("\\\\", "/") + "/");
        input = input.replaceAll("\\{NAME\\}", SyncSigns.getInstance().getId().replaceAll("[^\\w_-]", ""));

        return input;
    }


    public Connection getDatabase() {
        try {
            if (database.isClosed()) openDatabase();
        } catch (Exception ex) {
            WetSponge.getServer().getConsole().sendMessage(WSText.builder("[SyncSigns] ERROR LOADING DATABASE. PLEASE CHECK IT").color(EnumTextColor.RED).build());
            ex.printStackTrace();
            WetSponge.getServer().shutdown();
        }
        return database;
    }
}
