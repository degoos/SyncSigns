package com.degoos.syncsigns.util;


import com.degoos.syncsigns.SyncSigns;
import com.degoos.syncsigns.listener.SignChangeListener;
import com.degoos.syncsigns.listener.SignClickListener;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.WSEventManager;

public class ListenerUtils {

    private static boolean loaded;

    public static void load() {
        if (loaded) return;
        WSEventManager manager = WetSponge.getEventManager();
        SyncSigns instance = SyncSigns.getInstance();
        manager.registerListener(new SignChangeListener(), instance);
        manager.registerListener(new SignClickListener(), instance);
        loaded = true;
    }
}
